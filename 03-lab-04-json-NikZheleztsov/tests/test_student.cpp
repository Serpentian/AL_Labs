#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"
#include "nlohmann/json.hpp"
#include "student.h"
#include "table.h"

#include <any>
#include <vector>

TEST_CASE("Test methods of student class", "[student]")
{
    // Arrange 
    std::vector<Student> vec_students(3);
    const json json_students = R"(
		{
		  "items": [
		    {
		      "name": "Ivanov Petr",
		      "group": "1",
		      "avg": "4.25",
		      "debt": null
		    },
		    {
		      "name": "Sidorov Ivan",
		      "group": 31,
		      "avg": 4,
		      "debt": "C++"
		    },
		    {
		      "name": "Pertov Nikita",
		      "group": "IU8-31",
		      "avg": 3.33,
		      "debt": [
		        "C++",
		        "Linux",
		        "Network"
		      ]
		    }
		  ],
		  "_meta": {
		    "count": 3
		  }
		}
    )"_json;
    Table tab;
    tab.push_header({"name", "group", "avg", "debt"});
    
    // Act
    try {
        for (size_t i = 0; i < 3; ++i)
        {
            Student stud (json_students.at("items")[i]);
            vec_students[i] = stud;
            stud.push_to_table(tab);
        }
    } catch (...) {
        REQUIRE(false);
    }

    // Assert
    SECTION("Checking constructor of the student")
    {
        CHECK(vec_students[0].name == "Ivanov Petr");
        CHECK(std::any_cast<std::string>(vec_students[0].group) == "1");
        CHECK(std::any_cast<std::string>(vec_students[0].avg) == "4.25");
        CHECK(std::any_cast<std::nullptr_t>(vec_students[0].debt) == nullptr);

        CHECK(vec_students[1].name == "Sidorov Ivan");
        CHECK(std::any_cast<size_t>(vec_students[1].group) == 31);
        CHECK(std::any_cast<size_t>(vec_students[1].avg) == 4);
        CHECK(std::any_cast<std::string>(vec_students[1].debt) == "C++");

        CHECK(vec_students[2].name == "Pertov Nikita");
        CHECK(std::any_cast<std::string>(vec_students[2].group) == "IU8-31");
        CHECK(std::any_cast<double>(vec_students[2].avg) > 3.32); 
        CHECK(std::any_cast<double>(vec_students[2].avg) < 3.34);
        auto vec = std::any_cast<std::vector<std::string>>(vec_students[2].debt);
        CHECK(vec[0] == "C++");
        CHECK(vec[1] == "Linux");
        CHECK(vec[2] == "Network");

        CHECK(vec.size() == vec_students[2].get_debt_num());
    }

    SECTION("Checking data of the table")
    {
        auto vec = tab.get_tuples();
        CHECK(vec[0] == std::vector<std::string>({"Ivanov Petr", "1", "4.25", "null"}));
        CHECK(vec[1] == std::vector<std::string>({"Sidorov Ivan", "31", "4", "C++"}));
        CHECK(vec[2] == std::vector<std::string>({"Pertov Nikita", "IU8-31", "3.33", "3 items"}));
    }
}
