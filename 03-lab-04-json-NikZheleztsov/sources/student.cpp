// Copyright 2021 Nikita Zheleztsov

#include "./student.h"

#include "nlohmann/json.hpp"

Student::Student(const json& j) {
  json _name, _group, _avg, _debt;
  _name = j.at("name");
  _group = j.at("group");
  _avg = j.at("avg");
  _debt = j.at("debt");

  (_name.is_null()) ? name = nullptr : name = _name.get<std::string>();

  if (_group.is_null())
    group = nullptr;
  else if (_group.is_string())
    group = _group.get<std::string>();
  else
    group = _group.get<std::size_t>();

  if (_avg.is_null())
    avg = nullptr;
  else if (_avg.is_string())
    avg = _avg.get<std::string>();
  else if (_avg.is_number_float())
    avg = _avg.get<double>();
  else
    avg = _avg.get<std::size_t>();

  if (_debt.is_null()) {
    debt = nullptr;
  } else if (_debt.is_string()) {
    debt = _debt.get<std::string>();
  } else {
    debt = _debt.get<std::vector<std::string>>();
    debt_num = _debt.get<std::vector<std::string>>().size();
  }
}

void Student::push_to_table(Table& tab) {
  auto num(debt_num);
  auto to_string = [&num](std::any& a) -> std::string {
    try {
      std::any_cast<std::nullptr_t>(a);
    } catch (...) {
      if (a.type() == typeid(std::string)) {
        return std::any_cast<std::string>(a);
      } else if (a.type() == typeid(size_t)) {
        return std::to_string(std::any_cast<size_t>(a));
      } else if (a.type() == typeid(double)) {
        auto fl = std::to_string(std::any_cast<double>(a));
        size_t dot_place = fl.find('.');
        return fl.substr(0, dot_place + 3);
      }
    }

    if (num != 0) return std::string(std::to_string(num) + " items");

    return std::string("null");
  };

  std::vector<std::string> tuple(4);

  (typeid(name) != typeid(std::string)) ? tuple[0] = "null" : tuple[0] = name;

  tuple[1] = to_string(group);
  tuple[2] = to_string(avg);
  tuple[3] = to_string(debt);

  tab.push_tuple(tuple);
}
