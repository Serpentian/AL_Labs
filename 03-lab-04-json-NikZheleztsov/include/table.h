#ifndef TABLE_H
#define TABLE_H
#include <string>
#include <vector>

class Table {
 private:
  std::vector<std::pair<std::string, uint8_t>> header;
  std::vector<std::vector<std::string>> tuples;
  size_t m_width = 1;  // width of table
  void print_hor_line(std::ostream& os);

 public:
  void push_header(std::vector<std::string> names,
                   std::vector<uint8_t> width = {0});
  void push_tuple(std::vector<std::string> tup);
  inline size_t get_tuples_number() const { return tuples.size(); };

  inline const std::vector<std::vector<std::string>> get_tuples() const {
    return tuples;
  };

  friend std::ostream& operator<<(std::ostream& os, Table& tab);
};

#endif  // TABLE_H
