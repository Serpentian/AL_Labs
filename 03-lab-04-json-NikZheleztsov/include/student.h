#ifndef STUDENT_H
#define STUDENT_H

#include <any>
#include <string>

#include "nlohmann/json.hpp"
#include "table.h"

// don't need this structure at all
//
// but in the requirements of the lab
// there is the need to implement the 
// processing of different types
// (3-rd requirement), which is not 
// required when pushing to table
//
// Let's assume that in the future i'll
// need it )))

using json = nlohmann::json;

class Student
{
    size_t debt_num = 0;

    public:
    std::string name;
    std::any group;
    std::any avg;
    std::any debt;

    Student() {};
    explicit Student(const json& j);
    void push_to_table(Table& tab);
    size_t get_debt_num() const { return debt_num; }
};

#endif  // STUDENT_H
