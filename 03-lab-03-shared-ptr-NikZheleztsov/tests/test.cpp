// Copyright 2020 Your Name <your_email>

#include <gtest/gtest.h>
#include "shared_ptr.hpp"

TEST(SharedPtr, explicit_constructor) {
    SharedPtr<int> ptr(new int (5));
    EXPECT_EQ(5, *ptr.get());
}

TEST(SharedPtr, copy_constructor) {
    SharedPtr<int> ptr(new int(5));
    auto new_one(ptr);
    EXPECT_EQ(ptr.use_count(), 2);
    EXPECT_EQ(ptr.get(), new_one.get());
}

TEST(SharedPtr, move_operator) {
    SharedPtr<int> ptr(new int(5));
    auto new_one = std::move(ptr);
    ASSERT_EQ(ptr.get(), nullptr);
    ASSERT_EQ(ptr.use_count(), 0);
    ASSERT_EQ(*new_one, 5);
    ASSERT_EQ(new_one.use_count(), 1);
}

TEST(SharedPtr, bool_operator) {
    SharedPtr<int> ptr(new int(5));
    ASSERT_EQ(ptr, true);
}

TEST(SharedPtr, reset) {
    SharedPtr<int> ptr(new int(5));
    auto ptr2 = ptr;
    ptr.reset(new int(10));
    ASSERT_EQ(*ptr, 10);
    ASSERT_EQ(ptr.use_count(), 1);
}

TEST(SharedPtr, swap) {
    SharedPtr<int> ptr(new int (5));
    SharedPtr<int> ptr2(new int (10));
    ptr.swap(ptr2);
    ASSERT_EQ(ptr.use_count(), 1);
    ASSERT_EQ(ptr2.use_count(), 1);
    ASSERT_EQ(*ptr, 10);
    ASSERT_EQ(*ptr2, 5);
}
