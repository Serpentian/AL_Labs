#ifndef INCLUDE_CLIENT_HPP_
#define INCLUDE_CLIENT_HPP_

#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <cstdlib>
#include <iostream>
#include <map>
#include <string>

#include "status.hpp"

namespace beast = boost::beast;
namespace http = beast::http;
namespace net = boost::asio;
using tcp = net::ip::tcp;

class Client {
 public:
  Client() noexcept = delete;
  Client(const std::string& host, const std::string& port,
         const std::string& target) noexcept
      : resolver_(ioc_),
        stream_(ioc_),
        host_(host),
        port_(port),
        target_(target) {}

  Client(const Client&) = delete;
  Client& operator=(const Client&) = delete;
  ~Client() { Shutdown(); }

  Status Init() noexcept;
  Status SendPostRequst(const std::string& input) noexcept;
  Status GetResponse(std::string& resp) noexcept;
  void Shutdown() noexcept;

 private:
  net::io_context ioc_;
  tcp::resolver resolver_;
  beast::tcp_stream stream_;
  boost::system::error_code ec_;

  std::string host_;
  std::string port_;
  std::string target_;
};

#endif // INCLUDE_CLIENT_HPP_
