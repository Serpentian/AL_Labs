#include "client.hpp"

Status Client::Init() noexcept {
  auto const resolver_results_ = resolver_.resolve(host_, port_, ec_);
  stream_.connect(resolver_results_, ec_);

  if (ec_) {
    return Status(ec_.message());
  }

  return Status();
}

void Client::Shutdown() noexcept {
  try {
    stream_.socket().shutdown(tcp::socket::shutdown_both);
  } catch (...) {
  }
}

Status Client::SendPostRequst(const std::string& input) noexcept {
  try {
    std::string reqest = "{ \"input\": \"" + input + "\" }";
    http::request<http::string_body> req(http::verb::post, target_, 11);
    req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
    req.body() = http::string_body::value_type(reqest);
    req.prepare_payload();
    req.set(http::field::content_type, "application/json");
    req.set(http::field::host, host_);

    http::write(stream_, req);

  } catch (std::exception& e) {
    return Status(e.what());
  }

  return Status();
}

Status Client::GetResponse(std::string& resp) noexcept {
  try {
    http::response<http::string_body> res;
    beast::flat_buffer buffer;
    http::read(stream_, buffer, res);
    resp.clear();
    resp = res.body();
  } catch (std::exception& e) {
    return Status(e.what());
  }

  return Status();
}
