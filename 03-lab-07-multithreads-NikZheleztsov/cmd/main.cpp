// Copyright 2021 Nikita Zheleztsov

#include <getopt.h>

#include <boost/log/trivial.hpp>
#include <csignal>
#include <cstdint>
#include <iostream>
#include <thread>

#include "config.hpp"
#include "core.hpp"
#include "status.hpp"

static bool shutdown = false;
static std::unique_ptr<Core> core_ptr = nullptr;

void signal_handler(int signal) {
  if (signal == SIGINT) {
    shutdown = true;
  }
}

void listen_shutdown() {
  while (true) {
    if (shutdown) {
      if (core_ptr) {
        core_ptr->Shutdown();
      }
      break;
    }

    std::this_thread::sleep_for(std::chrono::seconds(2));
  }
}

void help() {
  printf("Usage: sha256 [OPTIONS]\n");
  printf("Launch sha256 program\n\n");
  printf(
      "Mandatory arguments to long options are mandatory "
      "for short options too.\n");
  printf("  -l, --log_dir=DIR     save logs to DIR\n");
  printf("  -r, --result=FILE     output json results to FILE\n");
  printf("  -t, --threads=N       launch with N threads\n");
  printf("  -h, --help            display this help and exit\n\n");
}

Status parse_command_line_arguments(int argc, char **argv) {
  auto failure = Status(Status::Code::FAILURE);
  std::string json = "./result.json", log = ".";
  uint8_t num = std::thread::hardware_concurrency();

  int c;
  while (true) {
    static struct option long_options[] = {
        {"log_dir", required_argument, 0, 'l'},
        {"result", required_argument, 0, 'r'},
        {"threads", required_argument, 0, 't'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}};

    int option_index = 0;
    c = getopt_long(argc, argv, "r:t:l:h", long_options, &option_index);

    if (c == -1) {
      break;
    }

    switch (c) {
      case 'r':
        json = optarg;
        break;

      case 't':
        num = std::atoi(optarg);
        break;

      case 'l':
        log = optarg;
        break;

      case 'h':
        help();
        return failure;

      default:
        return failure;
    }
  }

  Config::GetInstance().Init(num, json, log);
  return Status();
}

void run_program() {
  core_ptr = std::make_unique<Core>();
  core_ptr->Init();

  BOOST_LOG_TRIVIAL(info) << ": Launching sha256...";
  core_ptr->Run();
}

int main(int argc, char *argv[]) {
  if (!parse_command_line_arguments(argc, argv).ok()) {
    return EXIT_FAILURE;
  }

  run_program();
  std::signal(SIGINT, signal_handler);
  auto shutdown_thread = std::thread(&listen_shutdown);
  shutdown_thread.join();

  return EXIT_SUCCESS;
}
