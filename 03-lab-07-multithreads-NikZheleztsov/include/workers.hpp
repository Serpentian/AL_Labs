// Copyright 2021 Nikita Zheleztsov

#ifndef INCLUDE_WORKERS_HPP_
#define INCLUDE_WORKERS_HPP_

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstdint>
#include <memory>
#include <string>
#include <thread>

#include "logger.hpp"

class WorkersPool {
  std::atomic<bool> shutdown_;
  std::atomic<uint8_t> active_threads_num_;
  std::condition_variable cv_;
  std::mutex mutex_;
  uint8_t threads_num_;
  Logger& logger_;

  std::string getTimerString() const noexcept;
  void worker();

 public:
  typedef std::chrono::system_clock Clock;

  explicit WorkersPool(Logger& res, uint8_t num);
  WorkersPool(const WorkersPool&) = delete;
  WorkersPool& operator=(const WorkersPool&) = delete;
  ~WorkersPool() = default;

  void Run() noexcept;
  void Shutdown() noexcept;
};

#endif  // INCLUDE_WORKERS_HPP_
