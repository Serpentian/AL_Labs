// Copyright 2021 Nikita Zheleztsov

#ifndef INCLUDE_LOGGER_HPP_
#define INCLUDE_LOGGER_HPP_

#include <boost/log/trivial.hpp>
#include <memory>
#include <mutex>
#include <nlohmann/json.hpp>
#include <string>

class Logger {
  std::mutex mutex_;
  std::string log_dir_path_;
  std::string json_path_;
  std::unique_ptr<nlohmann::json> json;

  void initBoost() noexcept;

 public:
  Logger(const std::string& log_path, const std::string& json_path);
  void Add(std::string& time, std::string& data, std::string hash) noexcept;
  void Shutdown() noexcept;
};

#endif  // INCLUDE_LOGGER_HPP_
