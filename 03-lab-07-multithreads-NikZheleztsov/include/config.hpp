// Copyright 2021 Nikita Zheleztsov

#ifndef INCLUDE_CONFIG_HPP_
#define INCLUDE_CONFIG_HPP_

#include <cstdint>
#include <iostream>
#include <string>
#include <thread>

class Config {
 public:
  Config(const Config& conf) = delete;
  Config& operator=(const Config& other) = delete;

  static Config& GetInstance() noexcept {
    static Config instance;
    return instance;
  }

  void Init(uint8_t num, std::string& json, std::string log) {
    threads_num = num;
    json_path = json;
    log_dir = log;
  }

  const std::uint8_t& GetThreadsNum() const noexcept { return threads_num; }
  const std::string& GetJsonPath() const noexcept { return json_path; }
  const std::string& GetLogDir() const noexcept {
    std::cout << log_dir << std::endl;
    return log_dir;
  }

 private:
  uint8_t threads_num;
  std::string json_path;
  std::string log_dir;

  Config() = default;
  ~Config() = default;
};

#endif  // INCLUDE_CONFIG_HPP_
