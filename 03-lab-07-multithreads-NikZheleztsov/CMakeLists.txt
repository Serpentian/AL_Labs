cmake_minimum_required(VERSION 3.18)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

option(BUILD_TESTS "Build tests" ON)

set(HUNTER_CACHE_SERVERS
    "https://github.com/bmstu-iu8-cpp-sem-3/hunter-cache"
    CACHE STRING "Default cache server")

include("cmake/HunterGate.cmake")

huntergate(URL "https://github.com/cpp-pm/hunter/archive/v0.23.314.tar.gz" SHA1
           "95c47c92f68edb091b5d6d18924baabe02a6962a")

project(sha256 VERSION 0.1.0)
string(APPEND CMAKE_CXX_FLAGS " -pedantic -Werror -Wall -Wextra")
string(APPEND CMAKE_CXX_FLAGS " -Wshadow -Wnon-virtual-dtor")

hunter_add_package(Boost COMPONENTS system filesystem log)
hunter_add_package(nlohmann_json)
find_package(GTest CONFIG REQUIRED)
find_package(nlohmann_json CONFIG REQUIRED)
find_package(
  Boost
  CONFIG
  REQUIRED
  system
  filesystem
  log
  log_setup)

find_package(Threads REQUIRED)

add_library(${PROJECT_NAME}_Core STATIC
            ${CMAKE_CURRENT_SOURCE_DIR}/sources/core.cpp)

add_library(${PROJECT_NAME}_Workers STATIC
            ${CMAKE_CURRENT_SOURCE_DIR}/sources/workers.cpp)

add_library(${PROJECT_NAME}_Logger STATIC
            ${CMAKE_CURRENT_SOURCE_DIR}/sources/logger.cpp)

add_executable(${PROJECT_NAME} ${CMAKE_CURRENT_SOURCE_DIR}/cmd/main.cpp)

include_directories(
  "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>"
  "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/third-party>"
  "$<INSTALL_INTERFACE:include>")

target_link_libraries(
  ${PROJECT_NAME}_Logger Boost::log Boost::log_setup Boost::system
  Boost::filesystem nlohmann_json::nlohmann_json)

target_link_libraries(${PROJECT_NAME}_Workers ${PROJECT_NAME}_Logger
                      Threads::Threads)

target_link_libraries(${PROJECT_NAME}_Core ${PROJECT_NAME}_Workers
                      ${PROJECT_NAME}_Logger)

target_link_libraries(${PROJECT_NAME} ${PROJECT_NAME}_Core)

if(BUILD_TESTS)
  add_executable(tests ${CMAKE_CURRENT_SOURCE_DIR}/tests/example.cpp)
  target_link_libraries(tests GTest::gtest_main)
  enable_testing()
  add_test(NAME unit_tests COMMAND tests)
endif()

include(CPackConfig.cmake)

install(
  TARGETS ${PROJECT_NAME}
  EXPORT "${PROJECT_NAME}-targets"
  RUNTIME DESTINATION bin
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib
  INCLUDES
  DESTINATION include)

install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include/ DESTINATION include)
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/third-party/PicoSha2/pichosha2.h
        DESTINATION include)

install(
  EXPORT "${PROJECT_NAME}-targets"
  NAMESPACE "${PROJECT_NAME}::"
  DESTINATION "lib/cmake")
