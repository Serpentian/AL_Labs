#ifndef INCLUDE_BUILDER_HPP_
#define INCLUDE_BUILDER_HPP_

#include <boost/process.hpp>
#include <chrono>
#include <memory>
#include <mutex>
#include <system_error>
#include <thread>

#include "status.hpp"

using namespace boost::process;

class Builder {
 public:
  enum class Type { RELEASE, DEBUG };
  typedef std::chrono::seconds TimeDuration;
  typedef std::chrono::system_clock Clock;
  typedef Clock::time_point TimePoint;

  Builder() = default;
  Builder(const Builder&) = delete;
  Builder& operator=(const Builder&) = delete;
  ~Builder() { shutdown(); }

  int Run() noexcept;

 private:
  struct Config {
    Type build_type = Type::DEBUG;
    bool install = false;
    bool pack = false;
    TimeDuration timeout = TimeDuration(0);
  };

  int MakeTask() noexcept;
  int BuildTask() noexcept;
  int InstallTask() noexcept;
  int PackTask() noexcept;
  void shutdown() noexcept;

  std::unique_ptr<child> child_ = nullptr;
  std::mutex mutex_;

 public:
  static Config config;
};

#endif  // INCLUDE_BUILDER_HPP_
