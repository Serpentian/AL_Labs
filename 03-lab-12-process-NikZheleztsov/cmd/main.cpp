#include <getopt.h>

#include <cstring>
#include <iostream>

#include "builder.hpp"
#include "logger.hpp"
#include "status.hpp"

void help() {
  printf("Usage: builder [OPTIONS]\n");
  printf("Launch program for the simplification of CMake usage\n\n");
  printf(
      "Mandatory arguments to long options are mandatory "
      "for short options too.\n");
  printf("  -h, --help                      display this help and exit\n");
  printf("  -i, --install                   install project to '_install'\n");
  printf("  -p, --pack                      pack project to '*.tar.gz'\n");
  printf("  -c, --config=<Release|Debug>    change build configuration\n");
  printf("                                  (Debug by default)\n\n");
}

Status parse_command_line_arguments(int argc, char **argv) {
  auto failure = Status(Status::Code::FAILURE);

  int c;
  while (true) {
    static struct option long_options[] = {
        {"config", required_argument, 0, 'c'},
        {"install", no_argument, 0, 'i'},
        {"pack", no_argument, 0, 'p'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}};

    int option_index = 0;
    c = getopt_long(argc, argv, "c:iph", long_options, &option_index);

    if (c == -1) {
      break;
    }

    switch (c) {
      case 'c':
        if (!strcmp("Release", optarg)) {
          Builder::config.build_type = Builder::Type::RELEASE;
        } else if (strcmp("Debug", optarg)) {
          // if not debug and release
          LOG_ERROR("Unknown build type. Exiting...");
          return failure;
        }

        break;

      case 'i':
        Builder::config.install = true;
        break;

      case 'p':
        Builder::config.pack = true;
        break;

      case 'h':
        help();
        return failure;

      default:
        return failure;
    }
  }

  return Status();
}

int main(int argc, char *argv[]) {
  if (!parse_command_line_arguments(argc, argv).ok()) {
    return EXIT_FAILURE;
  }

  Builder builder;
  LOG_INFO("Builder was successfully initialized");
  int res = builder.Run();
  if (res) {
    LOG_ERROR("Building failed");
    return EXIT_FAILURE;
  }

  LOG_INFO("Building finished successfully");
  return EXIT_SUCCESS;
}
