#include "builder.hpp"

#include <async++.h>

#include <boost/iostreams/code_converter.hpp>
#include <boost/process.hpp>
#include <iostream>

#include "logger.hpp"

// init of the static builder config
Builder::Config Builder::config;

void Builder::shutdown() noexcept {
  if (child_) {
    try {
      child_->terminate();
    } catch (...) {
    }
  }
}

int Builder::Run() noexcept {
  auto make_task = async::spawn([this]() -> int { return this->MakeTask(); });

  auto build_task = make_task.then(
      [this](int stat) -> int { return stat ? stat : BuildTask(); });

  LOG_TRACE();
  int stat = build_task.get();
  if (stat) {
    return stat;
  }

  if (config.install) {
    auto install_task = async::spawn(
        [this, &stat]() -> int { return stat ? stat : InstallTask(); });
    stat = install_task.get();
  }

  // These one (install and pack) are not parallel tasks
  if (config.pack) {
    auto pack_task = async::spawn(
        [this, &stat]() -> int { return stat ? stat : PackTask(); });
    stat = pack_task.get();
  }

  if (config.timeout != TimeDuration(0)) {
    std::this_thread::sleep_for(config.timeout);
    LOG_WARN("Timer elapse. Terminating...");
    shutdown();
  }

  return stat;
}

int Builder::MakeTask() noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  std::string type = (config.build_type == Type::RELEASE) ? "Release" : "Debug";
  std::string cmd = boost::process::detail::posix::shell_path().native() +
                    " -c \"cmake -H. -B_builds -DCMAKE_INSTALL_PREFIX=_install "
                    "-DCMAKE_BUILD_TYPE=" +
                    type + "\"";

  LOG_INFO(cmd.c_str());

  try {
    child_ = std::make_unique<child>(cmd, std_out > stdout);
    child_->wait();
    return child_->exit_code();

  } catch (std::exception& e) {
    LOG_ERROR(e.what());
    return 1;
  }
}

int Builder::BuildTask() noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  std::string cmd = boost::process::detail::posix::shell_path().native() +
                    " -c \"cmake --build _builds\"";
  LOG_INFO(cmd.c_str());

  try {
    child_.reset(new child(cmd, std_out > stdout));
    child_->wait();
    return child_->exit_code();

  } catch (std::exception& e) {
    LOG_ERROR(e.what());
    return 1;
  }
}

int Builder::InstallTask() noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  std::string cmd = boost::process::detail::posix::shell_path().native() +
                    " -c \"cmake --build _builds --target install\"";
  LOG_INFO(cmd.c_str());

  try {
    child_.reset(new child(cmd, std_out > stdout));
    child_->wait();
    return child_->exit_code();

  } catch (std::exception& e) {
    LOG_ERROR(e.what());
    return 1;
  }
}

int Builder::PackTask() noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  std::string cmd = boost::process::detail::posix::shell_path().native() +
                    " -c \"cmake --build _builds --target package\"";
  LOG_INFO(cmd.c_str());

  try {
    child_.reset(new child(cmd, std_out > stdout));
    child_->wait();
    return child_->exit_code();

  } catch (std::exception& e) {
    LOG_ERROR(e.what());
    return 1;
  }
}
