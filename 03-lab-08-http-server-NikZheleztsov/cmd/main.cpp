#include <iostream>

#include "core/core.hpp"
#include "utils/logger.hpp"

static bool _shutdown = false;
static std::unique_ptr<Core> core_ptr = nullptr;

void signal_handler([[maybe_unused]] int signal) {
  _shutdown = true;
}

void listen_shutdown() {
  while (true) {
    if (_shutdown) {
      if (core_ptr) {
        core_ptr->Shutdown();
      }

      break;
    }

    std::this_thread::sleep_for(std::chrono::seconds(2));
  }
}

Status run(Config_cmd&& conf) {
  // Core initializing and running
  LOG_INFO("Initializing...");
  core_ptr = std::make_unique<Core>(std::move(conf));
  if (!core_ptr->Init().ok()) {
      LOG_ERROR("Core wasn't initialized. Exiting...");
      return Status(Status::Code::FAILURE);
  }
  LOG_INFO("Launching core...");
  if (!core_ptr->Run().ok()) {
      LOG_ERROR("Core cannot be launched. Exiting");
      return Status(Status::Code::FAILURE);
  }

  return Status();
}

int main(int argc, char **argv) {
  if (argc != 4) {
    std::cout << std::string(argv[0]) << " <address> <port> <suggest.json>\n";
    return EXIT_FAILURE;
  }

  Config_cmd cmd = {std::move(std::string(argv[1])),
                    std::move(std::string(argv[2])),
                    std::move(std::string(argv[3]))};

  if (!run(std::move(cmd)).ok()) {
    return EXIT_FAILURE;
  }

  // Shutdown it
  std::signal(SIGINT, signal_handler);
  auto shutdown_thread = std::thread(&listen_shutdown);
  shutdown_thread.join();
  return EXIT_SUCCESS;
}
