#include <google/protobuf/util/json_util.h>

#include <iostream>

#include "server.pb.h"

namespace util = google::protobuf::util;

int main() {
  Config conf;
  {
    auto ptr = conf.add_suggests();
    ptr->set_id("hel");
    ptr->set_name("hello");
    ptr->set_cost(100);
  }

  {
    auto ptr = conf.add_suggests();
    ptr->set_id("hel");
    ptr->set_name("hello");
    ptr->set_cost(100);
  }

  std::string out;
  util::MessageToJsonString(conf, &out);

  std::cout << out << std::endl;
}
