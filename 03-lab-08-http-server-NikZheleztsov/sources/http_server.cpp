// Copyright 2021 NikZheleztsov <mail>

#include "network/http_server.hpp"

#include <boost/asio.hpp>
#include <cstdlib>

#include "utils/logger.hpp"

namespace net = boost::asio;

Server::Server() noexcept
    : ioc_{1}, init_(false), exiting_(false), service_(nullptr) {}

void Server::RegisterService(std::unique_ptr<Service>&& service) noexcept {
  service_ = std::move(service);
}

Status Server::Init(const std::string& address,
                    const std::string& port) noexcept {
  try {
    address_ = net::ip::make_address(address);
    port_ = static_cast<unsigned short>(std::atoi(port.c_str()));

  } catch (const std::exception& e) {
    return Status(e.what());
  }

  init_ = true;
  return Status();
}

bool Server::isOk() const noexcept {
  return (init_ && service_);
}

Status Server::Run() {
  if (!isOk()) {
    LOG_ERROR("Exiting");
    return Status(Status::Code::FAILURE);
  }

  LOG_INFO("Launching the server...");
  thread_ = std::thread(&Server::launch_and_wait, this);
  return Status();
}

void Server::Shutdown() {
  if (!exiting_) {
    exiting_ = true;
    LOG_INFO("Shutting down the server...");

    ioc_.stop();
    if (thread_.joinable()) {
      thread_.join();
    }

    LOG_INFO("Server shutdown successfully...");
  }
}

void Server::launch_and_wait() noexcept {
  tcp::acceptor acceptor{ioc_, {address_, port_}};

  while (!exiting_) {
    // This will receive the new connection
    tcp::socket socket{ioc_};

    // Block until we get a connection
    acceptor.accept(socket);

    // Launch the session, transferring ownership of the socket
    std::thread{std::bind(&Server::session, this, std::move(socket))}.detach();
  }
}

void Server::session(tcp::socket& socket) noexcept {
  // This buffer is required to persist across reads
  beast::flat_buffer buffer;
  beast::error_code ec;
  bool close = false;
  send_lambda<tcp::socket> send(socket, close, ec);

  while (!exiting_) {
    // Read a request
    http::request<http::string_body> req;
    http::read(socket, buffer, req, ec);
    if (ec == http::error::end_of_stream) {
      break;
    }

    if (ec) {
      return LOG_ERROR("Read: %s", ec.message().c_str());
    }

    http::response<http::string_body> resp;
    http::response<http::empty_body> head_resp;
    auto status = service_->handle_request(req, resp, head_resp);
    if (head_resp.has_content_length()) {
      send(std::move(head_resp));
    } else {
      send(std::move(resp));
    }

    if (ec) {
      return LOG_ERROR("Write: %s", ec.message().c_str());
    }
  }

  // Send a TCP shutdown
  socket.shutdown(tcp::socket::shutdown_send, ec);
}
