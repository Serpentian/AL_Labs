// Copyright 2021 NikZheleztsov <mail>

#include "network/suggest_service.hpp"

#include <google/protobuf/util/json_util.h>

#include <boost/beast.hpp>
#include <cstring>

#include "server.pb.h"
#include "utils/logger.hpp"

namespace util = google::protobuf::util;
namespace beast = boost::beast;
namespace http = beast::http;

Status SuggestService::handle_request(
    http::request<http::string_body> &req,
    http::response<http::string_body> &resp,
    http::response<http::empty_body> &head_resp) {
  // for returning if smth is wrong
  Status failure = Status(Status::Code::FAILURE);

  // Returns a bad request response
  auto const bad_request = [&req](beast::string_view why) {
    http::response<http::string_body> res{http::status::bad_request,
                                          req.version()};
    res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
    res.set(http::field::content_type, "text/html");
    res.keep_alive(req.keep_alive());
    res.body() = std::string(why);
    res.prepare_payload();
    return res;
  };

  // Returns a not found response
  auto const not_found = [&req](beast::string_view target) {
    http::response<http::string_body> res{http::status::not_found,
                                          req.version()};
    res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
    res.set(http::field::content_type, "text/html");
    res.keep_alive(req.keep_alive());
    res.body() = "The resource '" + std::string(target) + "' was not found.";
    res.prepare_payload();
    return res;
  };

  // Returns a server error response
  [[maybe_unused]] auto const server_error = [&req](beast::string_view what) {
    http::response<http::string_body> res{http::status::internal_server_error,
                                          req.version()};
    res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
    res.set(http::field::content_type, "text/html");
    res.keep_alive(req.keep_alive());
    res.body() = "An error occurred: '" + std::string(what) + "'";
    res.prepare_payload();
    return res;
  };

  // Make sure we can handle the method
  if (req.method() != http::verb::post && req.method() != http::verb::head) {
    LOG_WARN("Cannot handle %s method. Only POST",
              req.method_string().to_string().c_str());
    resp = bad_request("Unknown HTTP-method");
    return failure;
  }

  if (req.target() != "/v1/api/suggest") {
    LOG_WARN("The resource %s was not found", req.target().to_string().c_str());
    resp = not_found(req.target());
    return failure;
  }

  // Parsing
  util::JsonParseOptions opt;
  opt.ignore_unknown_fields = false;
  opt.case_insensitive_enum_parsing = false;

  Request proto_req;
  {
    std::string json_request = req.body();
    auto status = util::JsonStringToMessage(json_request, &proto_req, opt);
    if (!status.ok()) {
      resp = bad_request(status.message().ToString().c_str());
      return failure;
    }
  }

  auto machine_resp = machine_.Query(proto_req);
  http::string_body::value_type body;

  {
    auto status = util::MessageToJsonString(machine_resp, &body);
    if (!status.ok()) {
      resp = not_found(status.message().ToString().c_str());
      return failure;
    }
  }

  auto const size = body.size();

  // Respond to HEAD request
  if (req.method() == http::verb::head) {
    http::response<http::empty_body> res{http::status::ok, req.version()};
    res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
    res.set(http::field::content_type, "application/json");
    res.content_length(size);
    res.keep_alive(req.keep_alive());
    head_resp = std::move(res);
    return Status();
  }

  // Respond to POST request
  http::response<http::string_body> res{
      std::piecewise_construct, std::make_tuple(std::move(body)),
      std::make_tuple(http::status::ok, req.version())};
  res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
  res.set(http::field::content_type, "application/json");
  res.content_length(size);
  res.keep_alive(req.keep_alive());
  resp = std::move(res);
  return Status();
}
