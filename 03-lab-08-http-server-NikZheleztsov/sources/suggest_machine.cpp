// Copyright 2021 NikZheleztsov <mail>

#include "core/suggest_machine.hpp"

#include <google/protobuf/util/json_util.h>

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iterator>

#include "utils/logger.hpp"
namespace util = google::protobuf::util;

Status SuggestMachine::Run() noexcept {
  if (file_name_.empty()) {
    LOG_ERROR("File name for SuggestMachine is empty");
    return Status(Status::Code::FAILURE);
  }

  LOG_INFO("Launching SuggestMachine...");
  read_thread_ = std::thread(&SuggestMachine::readThreadMain, this);
  return Status();
}

void SuggestMachine::readThreadMain() noexcept {
  LOG_INFO("SuggestMachine was successfully launched");
  std::unique_lock<std::mutex> lck(mutex_);
  auto stat = read();
  if (!stat.ok()) {
    LOG_ERROR("SuggestMachine thread is shutting down");
    return;
  }

  while (!exiting_) {
    TimePoint next_read_at = Clock::now() + READ_AFTER_;
    while (!exiting_) {
      TimePoint wait_until = Clock::now() + CHECK_EVERY_;
      if (next_read_at <= Clock::now()) {
        read();
        break;
      }

      cv_.wait_until(lck, wait_until);
    }
  }
}

Status SuggestMachine::read() noexcept {
  LOG_INFO("Reading data from file %s", file_name_.c_str());

  auto failure = Status(Status::Code::FAILURE);
  std::fstream input(file_name_, std::ios::in | std::ios::binary);

  if (!input.is_open()) {
    LOG_ERROR("Unable to open file %s", file_name_.c_str());
    return failure;
  }

  std::string json_data = std::string(std::istream_iterator<char>(input),
                                      std::istream_iterator<char>());
  Config conf;
  if (!util::JsonStringToMessage(json_data, &conf).ok()) {
    LOG_ERROR("Failed to parse file %s", file_name_.c_str());
  }

  for (auto x : conf.suggests()) {
    auto temp_pair = std::make_pair(x.id(), x);
    log_.insert(temp_pair);
  }

  LOG_INFO("Data was successfully read from %s", file_name_.c_str());
  input.close();
  return Status();
}

Response SuggestMachine::Query(const Request& req) noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  Response resp;

  std::pair<const_iter, const_iter> result = log_.equal_range(req.input());
  size_t size = std::distance(result.first, result.second);
  LOG_INFO("Found %zu elements with key %s", size, req.input().c_str());

  // not efficient one. Iterators constructor doesn't work
  std::vector<Config::ConfOne> data;
  for (auto it = result.first; it != result.second; ++it) {
    data.push_back(it->second);
  }

  std::sort(data.begin(), data.end(),
            [](Config::ConfOne& one, Config::ConfOne& other) {
              return one.cost() < other.cost();
            });

  for (size_t i = 0; i < data.size(); ++i) {
    auto add_sug = resp.add_suggests();
    auto temp = i + 1;
    Response::Suggestion suggest;
    suggest.set_text(data[i].name().c_str());
    suggest.set_position(std::move(temp));
    add_sug->CopyFrom(suggest);
  }

  return resp;
}

void SuggestMachine::Shutdown() noexcept {
  if (!exiting_) {
    LOG_INFO("Shutting down the SuggestMachine");
    exiting_ = true;
    cv_.notify_all();

    if (read_thread_.joinable()) {
      read_thread_.join();
    }
  }
}
