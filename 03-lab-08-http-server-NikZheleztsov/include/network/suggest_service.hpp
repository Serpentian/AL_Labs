// Copyright 2021 NikZheleztsov <mail>

#ifndef INCLUDE_SUGGEST_SERVICE_HPP_
#define INCLUDE_SUGGEST_SERVICE_HPP_

#include "core/suggest_machine.hpp"
#include "network/service.hpp"

class SuggestService : public Service {
 public:
  explicit SuggestService(SuggestMachine& ptr)
      : machine_(ptr) {}
  ~SuggestService() = default;

  Status handle_request(http::request<http::string_body>& req,
                        http::response<http::string_body>& resp,
                        http::response<http::empty_body>& head_resp) override;

 private:
  SuggestMachine& machine_;
};

#endif  // INCLUDE_SUGGEST_SERVICE_HPP_
