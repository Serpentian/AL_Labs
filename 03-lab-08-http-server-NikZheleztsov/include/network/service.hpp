// Copyright 2021 NikZheleztsov <mail>

#ifndef INCLUDE_SERVICE_HPP_
#define INCLUDE_SERVICE_HPP_

#include <boost/beast/http.hpp>

#include "utils/status.hpp"

namespace http = boost::beast::http;

class Service {
 public:
  virtual Status handle_request(
      http::request<http::string_body>& req,
      http::response<http::string_body>& resp,
      http::response<http::empty_body>& head_resp) = 0;

  virtual ~Service() = default;
};

#endif  // INCLUDE_SERVICE_HPP_
