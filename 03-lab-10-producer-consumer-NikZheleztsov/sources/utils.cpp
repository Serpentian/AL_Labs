#include "utils.hpp"

#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/error.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/log/trivial.hpp>

#include "root_certificates.hpp"

using tcp = boost::asio::ip::tcp;
namespace ssl = boost::asio::ssl;
namespace http = boost::beast::http;

namespace utils {

std::string getHost(std::string& url) noexcept {
  if (url.find("https://") == 0) {
    url = url.substr(8);
  }

  if (url.find("http:") == 0) {
    url = url.substr(7);
  }

  std::string result_host;
  for (unsigned i = 0; i < url.size(); ++i) {
    if ((url[i] == '/') || (url[i] == '?')) break;
    result_host += url[i];
  }
  return result_host;
}

std::string getTarget(std::string& url) noexcept {
  if (url.find("https:") == 0) {
    url = url.substr(8);
  }

  if (url.find("http:") == 0) {
    url = url.substr(7);
  }

  std::string result_target;
  unsigned pos = 0;
  // while (url[pos] != '/') { ++pos; }
  for (; pos < url.size(); ++pos) {
    if ((url[pos] == '/') || (url[pos] == '?')) break;
  }
  for (unsigned i = pos; i < url.size(); ++i) {
    result_target += url[i];
  }

  return result_target;
}

std::string getHtml(std::string& url) noexcept {
  auto host_ = getHost(url);
  auto target_ = getTarget(url);

  BOOST_LOG_TRIVIAL(info) << "Making request to host: " << host_
                          << " with target " << target_;
  try {
    auto const host = host_.c_str();
    auto const port = "443";
    auto const target = target_.c_str();
    int version = 11;

    ssl::context ctx{ssl::context::sslv23_client};
    load_root_certificates(ctx);

    boost::asio::io_context ioc;
    tcp::resolver resolver{ioc};
    ssl::stream<tcp::socket> stream{ioc, ctx};

    if (!SSL_set_tlsext_host_name(stream.native_handle(), host)) {
      boost::system::error_code ec{static_cast<int>(::ERR_get_error()),
                                   boost::asio::error::get_ssl_category()};
      throw boost::system::system_error{ec};
    }

    auto const results = resolver.resolve(host, port);
    boost::asio::connect(stream.next_layer(), results.begin(), results.end());
    stream.handshake(ssl::stream_base::client);

    http::request<http::string_body> req{http::verb::get, target, version};
    req.set(http::field::host, host);
    req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);

    http::write(stream, req);
    boost::beast::flat_buffer buffer;
    http::response<http::string_body> res;
    http::read(stream, buffer, res);

    boost::system::error_code ec;
    if (ec == boost::asio::error::eof) {
      ec.assign(0, ec.category());
    }

    if (ec) {
      throw boost::system::system_error{ec};
    }

    return res.body();

  } catch (std::exception const& e) {
    BOOST_LOG_TRIVIAL(error) << e.what();
  }

  return "";
}

// Implementation from gumbo examples
void search_for_links(GumboNode* node, std::vector<std::string>& vec) noexcept {
  if (node->type != GUMBO_NODE_ELEMENT) {
    return;
  }

  GumboAttribute* href;
  if (node->v.element.tag == GUMBO_TAG_A &&
      (href = gumbo_get_attribute(&node->v.element.attributes, "href"))) {
    vec.emplace_back(href->value);
  }

  GumboVector* children = &node->v.element.children;
  for (unsigned int i = 0; i < children->length; ++i) {
    search_for_links(static_cast<GumboNode*>(children->data[i]), vec);
  }
}

void search_for_pics(GumboNode* node, std::vector<std::string>& vec) noexcept {
  if (node->type != GUMBO_NODE_ELEMENT) {
    return;
  }

  GumboAttribute* src = nullptr;
  if (node->v.element.tag == GUMBO_TAG_IMG &&
      (src = gumbo_get_attribute(&node->v.element.attributes, "src"))) {
    std::string str = src->value;
    vec.emplace_back(str);
  }

  GumboVector* children = &node->v.element.children;
  for (unsigned int i = 0; i < children->length; ++i) {
    search_for_pics(static_cast<GumboNode*>(children->data[i]), vec);
  }
}

}  // namespace utils
