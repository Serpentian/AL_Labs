// Copyright 2021 NikZheleztsov <mail>

#include <gtest/gtest.h>

#include "stack/stack.h"
#include "stack/stack_2.h"

TEST(stack, interface) {
  Stack<int> stack;
  stack.push(5);
  ASSERT_EQ(5, stack.head());

  stack.push(3);
  ASSERT_EQ(3, stack.head());
  ASSERT_EQ(2, stack.size());

  stack.pop();
  ASSERT_EQ(5, stack.head());
  ASSERT_EQ(1, stack.size());
}

TEST(stack_2, interface) {
  Stack_2<int> st;
  st.push(4);
  st.push_emplace(13);
  ASSERT_EQ(13, st.head());
  ASSERT_EQ(2, st.size());

  auto var = st.pop();
  ASSERT_EQ(var, 13);
}
