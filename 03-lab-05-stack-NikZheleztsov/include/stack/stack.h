// Copyright 2021 NikZheleztsov <mail>

#ifndef STACK_H_
#define STACK_H_
#include "vector/vector.h"

// 1st task: not copyable but movable stack
//
// According to cppref container must provide:
// * back()
// * push_back()
// * pop_back()

template <class T, class Container = Vector<T>>
class Stack {
 public:
  typedef Container container_type;
  typedef typename Container::value_type value_type;
  typedef typename Container::size_type size_type;
  typedef typename Container::reference reference;
  typedef typename Container::const_reference const_reference;

  // Constructors
  explicit Stack(const Container& cont = Container()) : cont_(cont) {}
  Stack(Stack&& other) : cont_(std::move(other.cont_)){};
  Stack(const Stack&) = delete;

  // Operator =
  Stack& operator=(const Stack&) = delete;
  Stack& operator=(Stack&& other);

  // Destructor
  ~Stack() = default;

  // Interface
  void push(T&& value);
  void push(const T& value);
  void pop();
  const T& head() const;
  size_type size() const { return cont_.size(); }

 private:
  Container cont_;
};

template class Stack<int>;

#endif  // STACK_H_
