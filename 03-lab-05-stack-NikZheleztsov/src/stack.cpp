// Copyright 2021 NikZheleztsov <mail>

#include "stack/stack.h"

template <class T, class Container>
Stack<T, Container>& Stack<T, Container>::operator=(Stack&& other) {
  cont_ = std::move(other.cont_);
  return *this;
}

template <class T, class Container>
void Stack<T, Container>::push(T&& value) {
  cont_.push_back(std::move(value));
}

template <class T, class Container>
void Stack<T, Container>::push(const T& value) {
  cont_.push_back(value);
}

template <class T, class Container>
void Stack<T, Container>::pop() {
  cont_.pop_back();
}

template <class T, class Container>
const T& Stack<T, Container>::head() const {
  return cont_.back();
}
