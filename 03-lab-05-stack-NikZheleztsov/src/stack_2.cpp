// Copyright 2021 NikZheleztsov <mail>

#include "stack/stack_2.h"

template <class T, class Container>
Stack_2<T, Container>& Stack_2<T, Container>::operator=(Stack_2&& other) {
  cont_ = std::move(other.cont_);
  return *this;
}

template <class T, class Container>
void Stack_2<T, Container>::push(T&& value) {
  cont_.push_back(std::move(value));
}

template <class T, class Container>
T Stack_2<T, Container>::pop() {
  auto temp = cont_.back();
  cont_.pop_back();
  return temp;
}

